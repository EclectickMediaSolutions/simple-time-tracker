# Description

I wrote this in an afternoon to provide something simpler for time tracking. It's fully user friendly, and all time logs can be exported into your software of choice.

# Use

> Starting the software is easy, as long as you have `python3`!

![][1]

> You'll likely want to set your task details, as by default it is blank:

![][2]

> Once you've set your task details, you can easily start logging time in whatever folder you prefer. The script provides a simple directory change function to ensure you don't have to restart the software every time you want to change directories.

![][3]

> Now that you're where you need to be, its as simple as entering `s` to start your task.

![][4]

> Finally, lets review our work for today

![][5]

That's it! Pretty basic, eh?

[1]:https://i.imgur.com/U8pjryw.gif
[2]:https://i.imgur.com/faOvBzm.gif
[3]:https://i.imgur.com/oSkV7NU.gif
[4]:https://i.imgur.com/t3I36DT.gif
[5]:https://i.imgur.com/9qd5Nfk.gif
