#! /usr/bin/python3
""" Copyright Ariana Giroux, 2018 under the MIT license.
Implements a simple time tracking solution for freelancers.

Tracks time on task, task title, project, and billable.

Provides utility functions to display and modify logs."""
import csv
import time
import textwrap
import os
import sys


################################################################################
#                             UTILITY FUNCTIONS                                #

def _truncate(string, length):
    """ Shortens string from the centre, displaying missing text with an ellipses.

    `string` - The string to be truncated.
    `length` - The maximum length of the string.
    `returns` - A copy of string shortened to stay within max `length`.
    """
    if len(string) <= length:
        return string

    index_2 = length / 2
    index_1 = length - index_2 - 3
    return "%s...%s" % (string[:int(index_1)], string[-int(index_2):])


def _clear(log_path):
    """ Uses os.system to clear the screen and displays a "status bar"

    `log_path` - The path to the log file.
    `returns` - None, display function only.
    """

    os.system('cls' if os.name == 'nt' else 'clear')
    display_path = os.getcwd()[os.getcwd().index('arianagiroux'):]
    if display_path.count('/') > 1:
        display_path = display_path[display_path.index('/'):]

    print('%s\n' % textwrap.fill('Tracking in %s | Current File: %s\n\n'
                                 % (_truncate(display_path, length=25),
                                    _truncate(log_path, length=25)),
                                 width=70))


def _change_dir(log_path, path):
    """ Changes the scripts current working directory, or outputs os.system(`ls`)
    `log_path` - The path to the log file.
    `path` - The path to change directory to.
    `returns` - True if directory change successful.
    """
    if path != '-ls':
        path = os.path.expanduser(path)
        if not os.access(path, os.F_OK):
            print('\n    Not a valid path!')
            time.sleep(1)
            return

        os.chdir(path)
        print('\n    Changed directory to "%s"' % os.getcwd())
        time.sleep(1)
        return True

    else:
        _clear(log_path)
        os.system('echo files and folders in "$(pwd)":'
                  '&& ls -alFG | more')
        sys.stdout.write('\nPress CTRL-C to continue...')

        sys.stdout.flush()
        time.sleep(0.75)
        try:
            while 1:
                pass
        except KeyboardInterrupt:
            return False


def _get_table(log_path):
    """ Loads the CSV table from `log_path` and yields them one by one. """
    with open(log_path) as table_file:
        for line in csv.reader(table_file):
            yield line


################################################################################
#                                 MAIN LOGIC                                   #

def write(log_path, line):
    """ Writes `line` to the CSV file at `log_path`.
    `log_path` - The path to the log file.
    `line` - A line of data.
    """
    if os.access('logs', os.F_OK):
        with open(log_path, 'a+') as csv_file:
            csv.writer(csv_file).writerow(line)
    else:
        os.mkdir('logs')
        write(log_path, line)


def read(log_path, table_generator=_get_table):
    """ Displays a table to the user in a pretty formatted manner.
    `log_path` - The path to the log file.
    `table_generator` - A generator that provides lines of CSV. Defaults to
        `get_table`.
    """
    if not os.access(log_path, os.F_OK):
        print('No table present for current folder!')
        time.sleep(1)
        return

    string = "Displaying Table\n"
    total_time = 0
    billable_time = 0

    for line in table_generator(log_path):
        m, s = divmod(float(line[2]), 60)
        h, m = divmod(m, 60)
        total = '%2.2fh, %2.2fm, %2.2fs' % (h, m, s)

        if line[-1] == 'True':
            billable_time += float(line[2])

        total_time += float(line[2])

        string += '%s\n' % textwrap.fill(
            '.... %s-%s | Total: %s | Project: %s | Task: %s | Billable: %s'
            % (time.strftime('%m-%d-%y %H:%M', time.gmtime(float(line[0]))),
               time.strftime('%H:%M', time.gmtime(float(line[1]))),
               total, line[3], line[4], line[5]),
            width=77)

    m, s = divmod(total_time, 60)
    h, m = divmod(m, 60)
    string += '\nTotal time on task: %2.0fh, %2.0fm, %2.2fs' % (h, m, s)
    m, s = divmod(billable_time, 60)
    h, m = divmod(m, 60)
    string += ' | Total Billable time: %2.0fh, %2.0fm, %2.2fs' % (h, m, s)

    string += '\n\nPress CTRL-C to exit...'

    os.system('echo "%s" | more' % string)

    while 1:  # Wait for User
        pass


def _track_display_task(log_path):
    """ Displays the last completed task until the user interrupts.
    `log_path` - The path to the log file.
    """
    table = [entry for entry in _get_table(log_path)][-1]
    m, s = divmod(float(table[2]), 60)
    h, m = divmod(m, 60)
    total = '%2.2fh, %2.2fm, %2.2fs' % (h, m, s)

    while 1:
        _clear(log_path)

        print('Tracking stopped.')

        print('\n    Logged:')
        print('%s\n' % textwrap.fill('    .... %s-%s | Total: %s | Project: %s'
                                     '| Task: %s | Billable: %s' % (
                                         time.strftime('%m-%d-%y %H:%M',
                                                       time.gmtime(
                                                           float(table[0]))
                                                       ),
                                         time.strftime('%H:%M',
                                                       time.gmtime(
                                                           float(table[1]))
                                                       ),
                                         total, table[3], table[4], table[5]),
                                     width=77
                                     )
              )

        sys.stdout.write('\nPress CTRL-C to continue')
        sys.stdout.flush()
        time.sleep(0.75)
        for char in '...':
            sys.stdout.write(char)
            sys.stdout.flush()
            time.sleep(0.75)


def track(log_path, project_name, task_name, billable=False):
    """ Displays nice text until the user interrupts, then writes a line to
    `log_path`.

    `log_path` - The path to the log file.
    `project_name` - The user specified project name.
    `task_name` - The user supplied task name
    `billable` - True if user set `Interface.billable` to true.
    """
    start_time = time.time()
    # dot_counter = 0
    try:
        while 1:
            sys.stdout.write('Tracking task "%s"' % task_name)
            sys.stdout.flush()
            time.sleep(0.75)
            for char in '...':
                sys.stdout.write(char)
                sys.stdout.flush()
                time.sleep(0.75)
            _clear(log_path)

    except KeyboardInterrupt:
        end_time = time.time()

        write(log_path,
              [start_time, end_time, (end_time - start_time), project_name,
               task_name, billable])

        _track_display_task(log_path)


class Interface:
    """ Provides a user interface via STDIN/STDOUT. """
    def __init__(self):
        """ Prepares the initial task states and prepares the initial menu.

        Executes a while loop, getting user input and passing it to
        `self.command`. Waits for a `KeyboardInterrupt` to exit.
        """
        self.current_task = ''
        self.current_project = ''
        self.billable = False
        self.def_dict = {
            's': ('Start/continue task', self.start),
            'e': ('Edit task/project name', self.edit),
            'd': ('Display table', self.display),
            'c': ('Change the current directory', self.change_dir),
            'da': ('Display all tables', self.display_all)
        }

        try:
            while 1:
                _clear(self.LOG_PATH)
                print(str(self))
                user_input = str(input('=> '))
                time.sleep(0.25)
                self.command(user_input, self.def_dict)
        except KeyboardInterrupt:
            print('\n\n    Exiting.')
            time.sleep(1)
            exit()

    def __str__(self):
        """ Prepares a "status line" with the current task details and collects
        all registered commands in `self.def_dict`.
        """
        s = 'Current Task: "%s", Current Project: "%s", Billable: "%s"\n' \
            % (_truncate(self.current_task, length=10),
               _truncate(self.current_project, length=10),
               str(self.billable))

        for key in self.def_dict:
            s += '    %s = %s\n' % (key, self.def_dict[key][0])

        return s

    @property
    def LOG_PATH(self):
        """ Returns a path using the current date as the file name.
        """
        return 'logs/%s.csv' % time.strftime('%m-%d-%y')

    def command(self, user_input, def_dict):
        """ Attempts to call `def_dict[user_input]`, executing that "command".
        On `KeyError`, outputs error to user and returns.

        `user_input` - A string supplied by the user.
        `def_dict` - A dictionary containing `'command': function pairs`.
        """
        _clear(self.LOG_PATH)
        try:
            def_dict[user_input][1]()
        except KeyError:
            print('"%s" is not a valid option!' % user_input)
            time.sleep(1)
        except KeyboardInterrupt:
            pass

    def start(self):
        """ Calls `track` with all data. """
        track(self.LOG_PATH, self.current_project, self.current_task,
              self.billable)

    def edit(self):
        """ Implements a similar structure to init, entering its own nested loop.
        """
        def_dict = {
            'p': ('Project Name', self._edit_project),
            't': ('Task Name', self._edit_task),
            'b': ('Billable?', self._edit_billable)
        }
        try:
            while 1:
                _clear(self.LOG_PATH)
                print('Select Field (Press CTRL-C to save and exit!):')
                for key in def_dict:
                    print('    %s = %s' % (key, def_dict[key][0]))
                self.command(str(input('\n=> ')), def_dict)
        except KeyboardInterrupt:
            print('\n\n    Details Saved!')
            time.sleep(1)

    def _edit_project(self):
        """ Set `self.current_project` via user input.
        Called by `self.edit`."""
        self.current_project = str(input('Project Name: '))
        print('\n    Saved "Project Name" as "%s"' % self.current_project)
        time.sleep(1)

    def _edit_task(self):
        """ Set `self.current_task` via user input.
        Called by `self.edit`."""
        self.current_task = str(input('Task Name: '))
        print('\n    Saved "Task Name" as "%s"' % self.current_task)
        time.sleep(1)

    def _edit_billable(self):
        """ Set `self.billable` via user input.
        Called by `self.edit`."""
        user_input = str(input('Billable? (y/n) '))

        if user_input == 'y':
            self.billable = True
        elif user_input == 'n':
            self.billable = False
        else:
            print('Invalid selection!')
            self._edit_billable()

        print('\n    Saved "Billable" as "%s"' % self.billable)
        time.sleep(1)

    def display(self):
        """ Clears the screen and calls `read` """
        _clear(self.LOG_PATH)
        read(self.LOG_PATH)

    def display_all(self):
        """ Call `read`, supplying `self._read_all_generator`. """
        _clear(self.LOG_PATH)
        read(self.LOG_PATH, self._read_all_generator)

    def _read_all_generator(self, *args):
        """ Instead of only iterating through one file, iterates through all
        files in the logs directory.

        `*args` - is included to handle the single file that read() passes
        """
        for f in os.listdir('logs'):
            if f.count('.csv'):
                for line in _get_table('logs/%s' % f):
                    yield line

    def change_dir(self):
        """ Calls `_change_dir`. """
        try:
            while 1:
                _clear(self.LOG_PATH)
                _change_dir(self.LOG_PATH, str(input(
                    'Change to what directory? (-ls to run "ls -l", CTRL-C to'
                    'exit)\n=> ')))
            pass
        except KeyboardInterrupt:
            pass


if __name__ == '__main__':
    Interface()
